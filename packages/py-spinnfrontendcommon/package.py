# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


_JAR_URL = "https://github.com/SpiNNakerManchester/JavaSpiNNaker/releases/download/7.0.0/spinnaker-exe.jar"
_JAR_SHA256 = "2d909c7fb3aa15886acf26febb1bd48e25db0c347a231944aa6a5f86107bb55b"

class PySpinnfrontendcommon(PythonPackage):
    """This package provides utilities for specifying binary data
    algorithmically, and executing the specifications to produce the data."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNFrontEndCommon"
    pypi = "SpiNNFrontEndCommon/SpiNNFrontEndCommon-1!7.0.0.tar.gz"

    version("7.0.0", sha256="07539734ed0105472d06d655bbd92e149ef44c77c388fcca28857558faa6dd10")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-spinnman@7.0.0", type=("build", "run"))
    depends_on("py-spinnaker-pacman@7.0.0", type=("build", "run"))
    depends_on("py-spalloc@7.0.0", type=("build", "run"))
    depends_on("py-scipy@0.16.0:", type=("build", "run"))
    depends_on("py-ebrains-drive@0.5.1:", type=("build", "run"))

    depends_on("java@11:")

    resource(name="spinnaker-exe.jar", url=_JAR_URL, checksum=_JAR_SHA256, expand=False, placement="resource_root/JavaSpiNNaker/SpiNNaker-front-end/target")

    def install(self, spec, prefix):
        super(PySpinnfrontendcommon, self).install(spec, prefix)

        # Work out the python version installed, so we know where to put
        # the java code!
        python_version = spec["python"].version.up_to(2)
        python_lib = prefix.lib.join(f"python{python_version}")
        install_tree("resource_root", python_lib)
