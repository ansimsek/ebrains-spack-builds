# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNeuroglancerScripts(PythonPackage):
    """Conversion of images to the Neuroglancer pre-computed format"""

    homepage = "https://github.com/HumanBrainProject/neuroglancer-scripts"
    pypi     = "neuroglancer-scripts/neuroglancer-scripts-1.1.0.tar.gz"

    maintainers = ['Yann Leprince']

    version('1.1.0',    sha256='395a3565808136950a1e3296b43eeda13a0108d2286eba2b2924b33c3b3abc70')
    version('1.0.0',    sha256='524c0e3a3faebaf3952953f8097af347655bed3e70b3a4bc4043a6f216db7d28')
    version('1.0.0rc3', sha256='715811702fe4fc88210d54ea913787ab84e17b0ff4b7c62d30130f302a0a8335')
    version('0.3.0',    sha256='dd85c946f44f5211adf55eba04886e41dc36e9a94c723b966a8485c06d7ed253')
    version('0.2.0',    sha256='98d906347acf1be6a6cb49ed8753d51776ed38034f395b60de15bcf790e9533c')

    depends_on('python@3.5:',    type=('build', 'run'))

    depends_on('py-nibabel',      type=('build', 'run'))
    depends_on('py-numpy',        type=('build', 'run'))
    depends_on('py-pillow',       type=('build', 'run'))
    depends_on('py-requests',     type=('build', 'run'))
    depends_on('py-scikit-image', type=('build', 'run'))
    depends_on('py-tqdm',         type=('build', 'run'))

    # Not needed for the purpose siibra-python
    # depends_on('py-imagecodecs',  type=('build', 'run')) # TODO: had some issues
