# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyCsa(PythonPackage):
    """This provides basic utility functions and classes to other parts of
    SpiNNaker"s tooling. Nothing in here knows anything about SpiNNaker
    functionality."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNUtils"
    pypi = "csa/csa-0.1.12.tar.gz"

    version("0.1.12", sha256="15e7103f3df32871951785880b2fe6a18056768ba31a7cc053ba553717c4b2de")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-matplotlib", type=("build", "run"))
