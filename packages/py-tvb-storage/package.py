# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyTvbStorage(PythonPackage):
    """
    "The Virtual Brain" Project (TVB Project) has the purpose of offering modern tools to the Neurosciences community,
    for computing, simulating and analyzing functional and structural data of human brains, brains modeled at the level
    of population of neurons.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-storage/tvb-storage-2.9.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('2.9', 'b64ac69a1ffee3ed18664d60012511d0d37c2205c1c2ee1030b442d23fbfbe52')
    version('2.8.1', '4bbfd65ab1e150cf28a8206bcf5e30a8a092293b108b1bbb3365e6675f6d8837')
    version('2.7.3', '7f8b4d60313003e3c2b5b2f370f0a3a33ea5b22fde8b3f428eee7f2874673e4f')
    version('2.7.2', '501443ceb9f6374922a024b07ef09138f826fd0deaa69883c63dd0dff34fb16c')
    version('2.7', 'fd9e4d48517e91c0ad09de828668e27a9b19e5372d92fa3e3c8544b1cd8724a3')
    version('2.5', '749d1ef421a24a6f899ce8642c91660b4cccc72b1f00b1bbe08368e10f4f1159')
    version('2.4', '1af5586db3adeda02089a9cccd16dbfec1fc5bd1df1e4985fba5837efa72debc')
    version('2.3', 'e10bb40a486771703ba2776555ea5a453e619b07f10e4dea0d347043dee8f54b')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-setuptools', type='build')

    # install_requires
    depends_on('py-cryptography', type=('build', 'run'))
    depends_on('py-h5py', type=('build', 'run'))
    depends_on('py-kubernetes', type=('build', 'run'))
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-pyaescrypt', type=('build', 'run'))
    depends_on('py-requests', type=('build', 'run'))
    depends_on('py-tvb-library', type=('build', 'run'))

    depends_on('py-pytest', type='test')
    depends_on('py-decorator', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()