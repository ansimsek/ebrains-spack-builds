# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RDynutils(RPackage):
    """Provides common functionality for the 'dynverse' packages. 'dynverse' is created to support the development, execution, and benchmarking of trajectory inference methods. For more information, check out <https://dynverse.org>."""

    homepage = "https://cran.r-project.org/package=dynutils"
    cran = "dynutils"

    version("1.0.11", sha256="09ce9efda019e5c345ad6a7131f41f7fe9c8458c1b105191d8618416ea125619")

    depends_on("r-assertthat")
    depends_on("r-crayon")
    depends_on("r-desc")
    depends_on("r-dplyr")
    depends_on("r-magrittr")
    depends_on("r-matrix")
    depends_on("r-proxyc")
    depends_on("r-purrr")
    depends_on("r-rcpp")
    depends_on("r-remotes")
    depends_on("r-stringr")
    depends_on("r-tibble")



