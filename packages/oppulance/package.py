# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Oppulance(Package):
    """SDK for embedded processors on BrainScaleS-2"""

    homepage = "https://github.com/electronicvisions/oppulance"

    # PPU compiler dependencies
    depends_on('gettext')
    depends_on('zlib')
    depends_on('bison')
    depends_on('flex@2.6.4:')
    depends_on('m4')
    depends_on('texinfo')
    depends_on('wget')
    depends_on('gmp')

    releases = [
        {
            'version': '8.0-a5',
            'tag': 'ebrains-8.0-a5'
        },
        {
            'version': '8.0-a4',
            'tag': 'ebrains-8.0-a4'
        },
        {
            'version': '8.0-a3',
            'tag': 'ebrains-8.0-a3'
        },
        {
            'version': '8.0-a2',
            'tag': 'ebrains-8.0-a2'
        },
        {
            'version': '8.0-a1',
            'tag': 'ebrains-8.0-a1'
        },
        {
            'version': '7.0-rc1-fixup3',
            'tag': 'ebrains-7.0-rc1-fixup3'
        },
        {
            'version': '7.0-rc1-fixup2',
            'tag': 'ebrains-7.0-rc1-fixup2'
        },
        {
            'version': '7.0-rc1-fixup1',
            'tag': 'ebrains-7.0-rc1-fixup1'
        },
    ]

    for release in releases:
        version(
            release['version'],
            git='https://github.com/electronicvisions/oppulance',
            tag=release['tag'],
        )

        for res in ['binutils-gdb', 'gcc', 'newlib']:
            resource(
                name=res,
                git='https://github.com/electronicvisions/{}'.format(res),
                tag=release['tag'],
                placement=res
            )

    # defined by gcc/contrib/download_prerequisites
    resource(name='gmp-6.1.0.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/gmp-6.1.0.tar.bz2',
             sha256='498449a994efeba527885c10405993427995d3f86b8768d8cdf8d9dd7c6b73e8',
             expand=False,
             )
    resource(name='mpfr-3.1.4.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/mpfr-3.1.4.tar.bz2',
             sha256='d3103a80cdad2407ed581f3618c4bed04e0c92d1cf771a65ead662cc397f7775',
             expand=False,
             )
    resource(name='mpc-1.0.3.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/mpc-1.0.3.tar.gz',
             sha256='617decc6ea09889fb08ede330917a00b16809b8db88c29c31bfbb49cbf88ecc3',
             expand=False,
             )
    resource(name='isl-0.18.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/isl-0.18.tar.bz2',
             sha256='6b8b0fd7f81d0a957beb3679c81bbb34ccc7568d5682844d8924424a0dadcb1b',
             expand=False,
             )


    def install(self, spec, prefix):
        ln = which('ln')
        # move gcc resources into place
        for key in self.resources:
            for res in self.resources[key]:
                if not res.name.startswith(
                        ('gmp-', 'mpfr-', 'mpc-', 'isl-')):
                    continue
                assert(res.fetcher.stage.archive_file)
                ln('-sf', res.fetcher.stage.archive_file, 'gcc/')
        bash = which('bash')
        # extracts the gcc resources via gcc/contrib/download_prerequisites
        # (download is skipped if file exists)
        bash('gcc/ci/00_download_prerequisites.sh')
        bash('binutils-gdb/ci/00_build_install.sh')
        bash('gcc/ci/01_build_install_freestanding.sh')
        bash('newlib/ci/00_build_install.sh')
        bash('ci/00_build_install_libstdc++.sh')
        mkdirp(spec.prefix)
        install_tree('install/.', spec.prefix)

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install(self):
        ppu_gcc = which('powerpc-ppu-gcc')
        ppu_gcc('--version')
