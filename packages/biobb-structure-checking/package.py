# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbStructureChecking(PythonPackage):
    """Biobb_structure_checking performs a checking of the quality of a 
    3D structure intended to facilitate the setup of molecular dynamics 
    simulation of protein or nucleic acids systems"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_structure_checking"
    git = 'https://github.com/bioexcel/biobb_structure_checking.git'
    url = 'https://github.com/bioexcel/biobb_structure_checking/archive/refs/tags/v3.12.1.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('3.12.1', sha256='ef3e6fe5f7763e534c91fac00bf873c3d88bcca18be7a63c63608dceb36f3d40')

    # Dependencies
    depends_on('py-setuptools')
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-psutil', type=('build', 'run'))
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-biopython@1.78:1.80', type=('build', 'run'))

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_structure_checking')
