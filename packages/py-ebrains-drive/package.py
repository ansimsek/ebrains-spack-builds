# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyEbrainsDrive(PythonPackage):
    """Python client interface for EBRAINS Collaboratory Seafile storage"""

    homepage = "https://github.com/HumanBrainProject/ebrains-drive/blob/master/doc.md"
    git      = "https://github.com/HumanBrainProject/ebrains-drive.git"
    url = "https://pypi.org/packages/py3/e/ebrains_drive/ebrains_drive-0.5.1-py3-none-any.whl"

    version("0.6.0", sha256="8b734a0323ee2b3ee30f69d59d5fb93787da5928d152160df062f0049c152d3f", expand=False)
    version("0.5.1", sha256="67c0f82e8e6ec54f69d2cd1256eb0281ebd8d835d8f524e17877e14d7446d3e8", expand=False)
    version("0.5.0", sha256="c94747d00c28b891b0fb3aa743e25f77c4b63da137f68b400f01403cbcfb9ee9", expand=False)
    version("0.4.0", sha256="ea981f01e667c7c8f9cf63768f420a2a0915b034f3078767659a338dcf6c806f", expand=False)
    version("0.3.0", sha256="763f82549dd212002f271f3a8565965f7246adb898c814f180ab51f234e0026b", expand=False)
    version("0.2.0", sha256="1ea0096ee8e14df9f3ee2aff13cc0db77b74b6ce92599c2b3fb748961cebdb76", expand=False)

    depends_on('python@3.6.9:')
    depends_on('py-requests@2.26.0:', type=('build', 'run'))
    depends_on('py-tqdm', type=('build', 'run'), when='@0.6.0:')
