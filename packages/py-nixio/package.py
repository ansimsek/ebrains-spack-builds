# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNixio(PythonPackage):
    """Python library for the NIX data model.
    The NIX data model allows to store fully annotated scientific datasets,
    i.e. the data together with its metadata within the same container."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://github.com/G-Node/nixpy"
    pypi = "nixio/nixio-1.5.3.tar.gz"

    version("1.5.3", sha256="0ba7a65148297bd43a5ddaf143c4faad1c999771aa1d7e690aa0a5e31f368608")

    depends_on("py-setuptools", type="build")

    depends_on("py-six", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-h5py", type=("build", "run"))
