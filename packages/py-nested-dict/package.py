# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyNestedDict(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    homepage = "http://nested-dict.readthedocs.org"
    pypi     = "nested_dict/nested_dict-1.61.tar.gz"
    git      = "https://github.com/bunbun/nested-dict"

    maintainers = ['terhorstd', 'bunbun']

    version('1.61', sha256='de0fb5bac82ba7bcc23736f09373f18628ea57f92bbaa13480d23f261c41e771')

    depends_on('py-setuptools', type='build')
