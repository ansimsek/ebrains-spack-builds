### Summary

|               |                                        |
|---------------|----------------------------------------|
| Package       | <!-- Spack package name -->            |
| Version added | <!-- version number -->                |
| Contact point | <!-- maintainer GitLab handle -->      |
| KG entry      | <!-- insert link -->                   |


### Checks

<!-- please download the EBRAINS Software Quality checklist, fill it and replace the file below -->
* [ ] Software Quality Checklist: [SQ-Checklist.pdf](https://drive.ebrains.eu/d/6061531326d048308823/files/?p=%2FSQ-Checklist.pdf) (level: <!-- passing, silver, gold -->)
* [ ] Current maintainer is listed first in `package.py`
* [ ] No pinned dependency versions
* [ ] Post-installation tests are defined