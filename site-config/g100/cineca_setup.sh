#!/bin/bash
module load profile/global
module load spack/preprod-0.18.1-01
export SYSTEMNAME='g100'
#mkdir -p ../../../ebrains-spack-builds_cineca
#spack-python ../ymerge.py ../../spack.yaml spack.yaml > ../../../ebrains-spack-builds_cineca/spack.yaml
#ln -f -s $(pwd)/../../site-config ../../../ebrains-spack-builds_cineca/site-config
cd ..
cd ..
cd ..
spack env activate ebrains-spack-builds_cineca
#spack concretize -f 2>&1 | tee cineca_setup.log
rm ebrains-spack-builds_cineca/spack.lock
spack install --test root 2>&1 | tee cineca_install.log
