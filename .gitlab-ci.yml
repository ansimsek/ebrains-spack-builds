stages:
  - build
  - test

variables:
  BUILD_ENV_DOCKER_IMAGE: docker-registry.ebrains.eu/tc/ebrains-spack-build-env/base:devel
  SPACK_VERSION: v0.21.1
  SPACK_PATH_GITLAB: /mnt/spack_v0.21.1
  SYSTEMNAME: ebrainslab
  OC_PROJECT: jupyterhub

# ===================================================================
# LAB DEPLOYMENTS
# ===================================================================

# start an OpenShift Job that will build the Spack environment
.deploy-build-environment:
  stage: build
  script:
    # login and select project in openshift
    - oc login "$OPENSHIFT_SERVER" --token="$OPENSHIFT_TOKEN"
    - oc project $OC_PROJECT
    # create job description file
    - chmod a+x create_job.sh
    - ./create_job.sh $CI_PIPELINE_ID $BUILD_ENV_DOCKER_IMAGE $INSTALLATION_ROOT $SPACK_VERSION $SPACK_ENV $CI_COMMIT_BRANCH $RELEASE_NAME $LAB_KERNEL_ROOT
    - cat simplejob.yml
    # start the deploy job
    - oc create -f simplejob.yml
    # wait for job to finish to get the logs
    - while true; do sleep 300; x=$(oc get pods | grep simplejob${CI_PIPELINE_ID} | awk '{ print $3}'); if [ $x != "Running" ]; then break; fi; done 
    # # copy logs of failed packages locally, to keep as job artifacts
    # - oc rsync $(oc get pods -l job-name=simplejob${CI_PIPELINE_ID} -o name):/tmp ./ --include="*/" --include="spack/spack-stage/*/*.txt" --exclude="*"
    # - mv tmp/spack/spack-stage spack_logs
    # # also copy the spec of the created kernel, to keep as job artifacts
    # - LAB_KERNEL_PATH = $LAB_KERNEL_ROOT/$(echo "$RELEASE_NAME" | tr '[:upper:]' '[:lower:]')
    # - oc rsync $(oc get pods -l job-name=simplejob${CI_PIPELINE_ID} -o name):$LAB_KERNEL_PATH ./
    # - mv .$LAB_KERNEL_PATH kernel_specs
    # if spack install has failed, fail the pipeline
    - oc logs jobs/simplejob${CI_PIPELINE_ID} | tee log.txt
    - if [ $(cat log.txt | grep "No module available for package" | wc -l) -gt 0 ]; then exit 1; fi;
    # delete the job from OpenShift as we have the logs here
    - oc delete job simplejob${CI_PIPELINE_ID} || true
  tags:
    - shell-runner
  # artifacts:
  #   paths:
  #     - spack_logs
  #     - kernel_specs
  #   when: always

# -------------------------------------------------------------------
# Lab deployment target environments: dev and prod Lab instances
# -------------------------------------------------------------------

# deploy to a dev lab environment
.deploy-dev-server:
  extends: .deploy-build-environment
  variables:
    LAB_KERNEL_ROOT: /srv/jupyterlab_kernels/int
    INSTALLATION_ROOT: /srv/test-build-2402

# deploy to a prod lab environment
.deploy-prod-server:
  extends: .deploy-build-environment
  variables:
    LAB_KERNEL_ROOT: /srv/jupyterlab_kernels/prod
    INSTALLATION_ROOT: /srv/main-spack-instance-2402

# deploy to the dev lab environment at CSCS
.deploy-dev-server-cscs:
  extends: .deploy-dev-server
  variables:
    OPENSHIFT_SERVER: $CSCS_OPENSHIFT_DEV_SERVER
    OPENSHIFT_TOKEN: $CSCS_OPENSHIFT_DEV_TOKEN
    BUILD_ENV_DOCKER_IMAGE: docker-registry.ebrains.eu/tc/ebrains-spack-build-env/okd:okd_23.06
    OC_PROJECT: jupyterhub-int
  resource_group: shared-NFS-mount-dev-cscs
  tags:             # this is just to ensure that the two jobs will run on different runners
    - read-write    # to avoid issues with common environment variables
    - shell-runner

# deploy to the prod lab environment at CSCS
.deploy-prod-server-cscs:
  extends: .deploy-prod-server
  variables:
    OPENSHIFT_SERVER: $CSCS_OPENSHIFT_PROD_SERVER
    OPENSHIFT_TOKEN: $CSCS_OPENSHIFT_PROD_TOKEN
    BUILD_ENV_DOCKER_IMAGE: docker-registry.ebrains.eu/tc/ebrains-spack-build-env/okd:okd_23.06
  resource_group: shared-NFS-mount-prod-cscs
  tags:             # this is just to ensure that the two jobs will run on different runners
    - read-write    # to avoid issues with common environment variables
    - shell-runner

# deploy to the dev lab environment at CINECA
.deploy-dev-server-cineca:
  extends: .deploy-dev-server
  variables:
    OPENSHIFT_SERVER: $CINECA_K8S_DEV_SERVER
    OPENSHIFT_TOKEN: $CINECA_K8S_DEV_TOKEN
  resource_group: shared-NFS-mount-dev-cineca
  tags:             # this is just to ensure that the two jobs will run on different runners
    - read-only     # to avoid issues with common environment variables
    - shell-runner

# deploy to the prod lab environment at JSC
.deploy-prod-server-jsc:
  extends: .deploy-prod-server
  variables:
    OPENSHIFT_SERVER: $JSC_K8S_PROD_SERVER
    OPENSHIFT_TOKEN: $JSC_K8S_PROD_TOKEN
  resource_group: shared-NFS-mount-prod-jsc
  tags:             # this is just to ensure that the two jobs will run on different runners
    - read-only     # to avoid issues with common environment variables
    - shell-runner

# -------------------------------------------------------------------
# Release types: test, experimental and official releases
# -------------------------------------------------------------------

# deploy int release (latest changes) to dev env to be tested before release to production
.deploy-int-release:
  variables:
    SPACK_ENV: test
    RELEASE_NAME: EBRAINS-test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_NAMESPACE =~ /platform\/esd/ && $CI_PIPELINE_SOURCE != "schedule"

# deploy the experimental release of tools once a week from latest working version of int release 
.deploy-exp-release:
  variables:
    SPACK_ENV: experimental
    RELEASE_NAME: EBRAINS-experimental

# deploy the experimental release to dev env
.deploy-exp-dev-release:
  extends: .deploy-exp-release
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"  && $DEPLOYMENT == "dev"

# deploy the experimental release to prod env
.deploy-exp-prod-release:
  extends: .deploy-exp-release
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"  && $DEPLOYMENT == "prod"

# deploy the production release of tools
.deploy-prod-release:
  variables:
    SPACK_ENV: ebrains-24-04
    RELEASE_NAME: EBRAINS-24.04
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^ebrains/
      when: manual

# -------------------------------------------------------------------
# Lab deployment jobs
# -------------------------------------------------------------------

# deploy int release to dev environment at CSCS
deploy-int-release-dev-cscs:
  extends:
    - .deploy-int-release
    - .deploy-dev-server-cscs

# deploy int release to dev environment at CINECA
deploy-int-release-dev-cineca:
  extends:
    - .deploy-int-release
    - .deploy-dev-server-cineca

# deploy exp release to dev environment at CSCS
deploy-exp-release-dev-cscs:
  extends:
    - .deploy-exp-dev-release
    - .deploy-dev-server-cscs

# deploy exp release to dev environment at CINECA
deploy-exp-release-dev-cineca:
  extends:
    - .deploy-exp-dev-release
    - .deploy-dev-server-cineca

# deploy exp release to prod environment at CSCS
deploy-exp-release-prod-cscs:
  extends:
    - .deploy-exp-prod-release
    - .deploy-prod-server-cscs

# deploy exp release to prod environment at JSC
deploy-exp-release-prod-jsc:
  extends:
    - .deploy-exp-prod-release
    - .deploy-prod-server-jsc

# deploy prod release to prod environment at CSCS
deploy-prod-release-prod-cscs:
  extends:
    - .deploy-prod-release
    - .deploy-prod-server-cscs

# deploy prod release to prod environment at JSC
deploy-prod-release-prod-jsc:
  extends:
    - .deploy-prod-release
    - .deploy-prod-server-jsc

# ===================================================================
# GITLAB RUNNER DEPLOYMENTS
# ===================================================================

# run test build job on any gitlab runner, trying to build latest changes
# with persistent, read-only deployment as upstream
build-spack-env-on-runner:
  stage: build
  tags:
    - docker-runner
    - read-only
  image: $BUILD_ENV_DOCKER_IMAGE
  variables:
    SPACK_DEV_ENV: ebrains-dev
    SPACK_JOBS: 2
  script:
    # deactivate environment views (we don't need them for the test build-job)
    - >
        echo "  view: False" >> $CI_PROJECT_DIR/site-config/$SYSTEMNAME/spack.yaml
    # run installation script
    - . install_spack_env.sh $SPACK_JOBS $CI_PROJECT_DIR $SPACK_VERSION $CI_PROJECT_DIR $SPACK_DEV_ENV $SPACK_PATH_GITLAB
    # re-activate envionment and run tests
    - spack env activate $SPACK_DEV_ENV
    # TODO: run all tests when test dependency issue is fixed
    # - spack test run -x wf-brainscales2-demos wf-multi-area-model
  after_script:
    - mkdir -p $CI_PROJECT_DIR/spack_logs/installed $CI_PROJECT_DIR/spack_logs/not_installed
      # for succesfully installed packages: keep the spack logs for any package modified during this CI job
    - PKG_DIR=$CI_PROJECT_DIR/spack/opt/spack/linux-ubuntu20.04-x86_64/gcc-10.3.0
    - if cd $PKG_DIR; then find . \( -name ".spack" -o -name ".build" \) -exec cp -r --parents "{}" $CI_PROJECT_DIR/spack_logs/installed \;; fi
      # for not succesfully installed packages: also keep the spack logs for any packages that failed
    - if cd /tmp/$(whoami)/spack-stage/; then find . -maxdepth 2 \( -name "*.txt" -o -name ".install_time_tests" \) -exec cp -r --parents "{}" $CI_PROJECT_DIR/spack_logs/not_installed \;; fi
    # - if [ -d /tmp/spack_tests ]; then mv /tmp/spack_tests $CI_PROJECT_DIR; fi
  artifacts:
    paths:
      - spack_logs
      # - spack_tests
    when: always
  timeout: 2 days
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule" && $CI_PIPELINE_SOURCE != "merge_request_event"

# update gitlab-runner upstream (read-only) installation
sync-gitlab-spack-instance:
  stage: build
  tags:
    - docker-runner
    - read-write
  image: $BUILD_ENV_DOCKER_IMAGE
  variables:
    SPACK_REPO_PATH: $SPACK_PATH_GITLAB/ebrains-spack-builds
    SPACK_JOBS: 4
  script:
    - SPACK_NFS_ENV=${CI_COMMIT_BRANCH//./-}
    # create spack dir if it doesn't exist
    - mkdir -p $SPACK_PATH_GITLAB
    # get latest state of EBRAINS repo
    - rm -rf $SPACK_REPO_PATH && cp -r $CI_PROJECT_DIR $SPACK_REPO_PATH
    # run installation script
    - . install_spack_env.sh $SPACK_JOBS $SPACK_PATH_GITLAB $SPACK_VERSION $SPACK_REPO_PATH $SPACK_NFS_ENV
    # create kernel spec, so that the environment can be used in gitlab CI jobs
    - RELEASE_NAME=$(case $CI_COMMIT_BRANCH in experimental_rel) echo ebrains-experimental;; ebrains*) echo ${CI_COMMIT_BRANCH:0:10}.${CI_COMMIT_BRANCH:11};; *) echo $CI_COMMIT_BRANCH;; esac);
    - . create_JupyterLab_kernel.sh $SPACK_PATH_GITLAB $SPACK_NFS_ENV $RELEASE_NAME /mnt/ebrains_env
  after_script:
    - mkdir -p $CI_PROJECT_DIR/spack_logs/installed $CI_PROJECT_DIR/spack_logs/not_installed
      # for succesfully installed packages: keep the spack logs for any package modified during this CI job
      # (we use repo.yaml, that is modified at each start of the pipeline, as a reference file)
    - PKG_DIR=$SPACK_PATH_GITLAB/spack/opt/spack/linux-ubuntu20.04-x86_64/gcc-10.3.0
    - if cd $PKG_DIR; then find . -newer $SPACK_REPO_PATH/repo.yaml \( -name ".spack" -o -name ".build" \) -exec cp -r --parents "{}" $CI_PROJECT_DIR/spack_logs/installed \;; fi
      # for not succesfully installed packages: also keep the spack logs for any packages that failed
    - if cd /tmp/$(whoami)/spack-stage/; then find . -maxdepth 2 \( -name "*.txt" -o -name ".install_time_tests" \) -exec cp -r --parents "{}" $CI_PROJECT_DIR/spack_logs/not_installed \;; fi
  artifacts:
    paths:
      - spack_logs
    when: always
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "experimental_rel" || $CI_COMMIT_BRANCH =~ /^ebrains/) && $CI_PROJECT_NAMESPACE =~ /platform\/esd/ && $CI_PIPELINE_SOURCE != "schedule"
      when: manual

# run (scheduled) standalone tests for environment
test-gitlab-spack-instance:
  stage: test
  tags:
    - docker-runner
    - read-write
  image: $BUILD_ENV_DOCKER_IMAGE
  variables:
    SPACK_REPO_PATH: $SPACK_PATH_GITLAB/ebrains-spack-builds
    SPACK_USER_CACHE_PATH: $SPACK_PATH_GITLAB/spack/.spack
    SPACK_USER_CONFIG_PATH: $SPACK_PATH_GITLAB/spack/.spack
  script:
    - SPACK_NFS_ENV=${CI_COMMIT_BRANCH//./-}
    - source $SPACK_PATH_GITLAB/spack/share/spack/setup-env.sh
    - spack env activate $SPACK_NFS_ENV
    # TODO: run all tests when test dependency issue is fixed
    - spack test run -x wf-brainscales2-demos wf-multi-area-model
  after_script:
    - if [ -d /tmp/spack_tests ]; then mv /tmp/spack_tests $CI_PROJECT_DIR; fi
  artifacts:
    paths:
      - spack_tests
    when: always
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $TEST_DEPLOYMENT == "true"
